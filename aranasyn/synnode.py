#!/usr/bin/python
# -*- coding=utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        synNode
# Purpose:     representat data analyzed given by morphoanalyzer Qalsadi then by syntaxic analyzer
#
# Author:      Taha Zerrouki (taha.zerrouki[at]gmail.com)
#
# Created:     19-09-2012
# Copyright:   (c) Taha Zerrouki 2012
# Licence:     GPL
#-------------------------------------------------------------------------------
if __name__ == "__main__":
	import sys
	sys.path.append('../lib')

import syn_const
import stemmedsynword
import pyarabic.araby as araby
class synNode:
	"""
	synNode represents the regrouped data resulted from the morpholocigal analysis
	"""
	def __init__(self, caseList, order=-1):
		"""
		Create the synNode  from a list of StemmedSynword cases
		"""
		self.caseCount=len(caseList);
		""" the number of syntaxtical cases """

		self.verbCount=0;
		""" the number of syntaxtical verb cases """

		self.nounCount=0;
		""" the number of syntaxtical noun cases """

		self.stopwordCount=0;
		""" the number of syntaxtical stopword cases """
		
		self.word=''
		""" The unstemmed word """
		self.originals=set()
		self.nounOriginals=set()
		self.stopwordOriginals=set()		
		self.verbOriginals=set()
		self.pounctOriginals=set()		
		""" The list of original words"""
		if caseList:
			self.word=caseList[0].getWord();
		for case in caseList:
			self.word
			if case.isVerb():
				self.verbOriginals.add(case.getOriginal());
			elif case.isNoun():
				self.nounOriginals.add(case.getOriginal());				
			elif case.isStopWord():
				self.stopwordOriginals.add(case.getOriginal());
			elif case.isPounct():
				self.pounctOriginals.add(case.getOriginal());				
				
			self.originals.add(case.getOriginal());
		self.verbCount     = len(self.verbOriginals);
		self.nounCount     = len(self.nounOriginals);
		self.stopwordCount = len(self.stopwordOriginals);				
		self.pounctCount = len(self.pounctOriginals);			
		
	######################################################################
	#{ Attributes Functions
	######################################################################		

	def setCaseCount(self, count):
		"""
		Set the case count.
		@param count: the number of stemmed word  cases
		@tyep count: integer;
		"""
		self.caseCount=count;
	def getCaseCount(self):
		"""
		get the case count.
		@return: the number of stemmed word  cases
		@tyep count: integer;
		"""
		return self.caseCount;
	def setVerbCount(self, count):
		"""
		Set the verb count.
		@param count: the number of stemmed word cases as  verbs
		@tyep count: integer;
		"""
		self.verbCount=count;
	def getVerbCount(self):
		"""
		get the verb count.
		@return: the number of stemmed word cases as verbs
		@tyep count: integer;
		"""
		return self.verbCount;
		
	def setNounCount(self, count):
		"""
		Set the noun count.
		@param count: the number of stemmed word cases as  nouns
		@tyep count: integer;
		"""
		self.nounCount=count;
	def getNounCount(self):
		"""
		get the noun count.
		@return: the number of stemmed word cases as nouns
		@tyep count: integer;
		"""
		return self.nounCount;
	def setStopwordCount(self, count):
		"""
		Set the stopword count.
		@param count: the number of stemmed word cases as  stopwords
		@tyep count: integer;
		"""
		self.stopwordCount=count;
	def getStopwordCount(self):
		"""
		get the stopword count.
		@return: the number of stemmed word cases as stopwords
		@tyep count: integer;
		"""
		return self.stopwordCount;
	def getWord(self,):
		"""
		Get the input word given by user
		@return: the given word.
		@rtype: unicode string
		"""
		return self.word;
	def setWord(self,newword):
		"""
		Set the input word given by user
		@param newword: the new given word.
		@type newword: unicode string
		"""
		self.word = newword;
	def getOriginal(self,):
		"""
		Get the original forms of the input word
		@return: the given original.
		@rtype: unicode string
		"""
		return self.originals;

	def setOriginal(self,neworiginal):
		"""
		Set the original words
		@param neworiginal: the new given original.
		@type neworiginal: unicode string list
		"""
		self.originals = neworiginal;

	######################################################################
	#{ Tags extraction Functions
	######################################################################		
	def hasVerb(self,):
		"""
		Return if all cases are verbs.
		@return:True if the node has verb in one case at least.
		@rtype:boolean
		"""
		return self.verbCount>0;
	def hasNoun(self,):
		"""
		Return if all cases are nouns.
		@return:True if the node has noun in one case at least.
		@rtype:boolean
		"""
		return self.nounCount>0;

	def hasStopword(self,):
		"""
		Return if all cases are stopwords.
		@return:True if the node has stopword in one case at least.
		@rtype:boolean
		"""
		return self.stopwordCount>0;
	def hasPounct(self,):
		"""
		Return if all cases are pounctuations
		@return:True if the node has pounctation in one case at least.
		@rtype:boolean
		"""
		return self.pounctCount>0;		
	def isVerb(self,):
		"""
		Return if all cases are verbs.
		@return:True if the node is verb in alll cases.
		@rtype:boolean
		"""
		return self.verbCount==self.caseCount;
	def isNoun(self,):
		"""
		Return if all cases are nouns.
		@return:True if the node is noun in alll cases.
		@rtype:boolean
		"""
		return self.nounCount==self.caseCount;

	def isStopword(self,):
		"""
		Return if all cases are stopwords.
		@return:True if the node is stopword in alll cases.
		@rtype:boolean
		"""
		return self.stopwordCount==self.caseCount;
	def isPounct(self,):
		"""
		Return if all cases are pounctuations
		@return:True if the node is pounctation in alll cases.
		@rtype:boolean
		"""
		return self.pounctCount==self.caseCount;		
	def isMostVerb(self,):
		"""
		Return True if most  cases are verbs.
		@return:True if the node is verb in most cases.
		@rtype:boolean
		"""
		
		return self.verbCount> self.nounCount and self.verbCount> self.stopwordCount;
	def isMostNoun(self,):
		"""
		Return True if most  cases are nouns.
		@return:True if the node is noun in most cases.
		@rtype:boolean
		"""
		return self.nounCount >self.verbCount  and self.nounCount> self.stopwordCount;

	def isMostStopword(self,):
		"""
		Return True if most cases are stopwords.
		@return:True if the node is stopword in most cases.
		@rtype:boolean
		"""
		return self.stopwordCount >self.verbCount  and self.stopwordCount> self.nounCount;

	def getWordType(self,):
		"""
		Return the word type.
		@return:the word type or mosttype.
		@rtype:string
		"""
		if self.isNoun():
			return 'noun';
		elif self.isVerb():
			return 'verb';
		elif self.isStopword():
			return 'stopword';
		elif self.isPounct():
			return 'pounct';			
		elif self.isMostNoun():
			return 'mostnoun';
		elif self.isMostVerb():
			return 'mostverb';
		elif self.isMostStopword():
			return 'moststopword';
		else:
			return 'ambiguous'
	######################################################################
	#{ Tags  Functions
	######################################################################		

	# def isInitial(self):
		# """
		# Return True if the word mark the begin of next sentence.
		# @return: direct initial.
		# @rtype: True/False;
		# """
		# return self.tagInitial;


	# def isDirectJar(self):
		# """
		# Return True if the word is a direct Jar.
		# @return: direct Jar.
		# @rtype: True/False;
		# """	

		# return self.tagDirectJar;

	# def isDirectVerbNaseb(self):
		# """
		# Return True if the word is a direct Naseb of verb.
		# @return: direct Naseb.
		# @rtype: True/False;
		# """	
		# return self.tagDirectVerbNaseb;


	# def isDirectJazem(self,):
		# """
		# Return True if the word is a direct Jazem.
		# @return: direct Jazem.
		# @rtype: True/False;
		# """

		# return self.tagDirectJazem;

	# def isDirectNaseb(self):
		# """
		# Return True if the word is a direct Naseb of noun.
		# @return: direct Naseb of noun.
		# @rtype: True/False;
		# """
		# return self.tagDirectNaseb;




	# def isDirectRafe3(self):
		# """
		# Return True if the word is a direct Rafe3.
		# @return: direct Rafe3.
		# @rtype: True/False;
		# """
		# return self.tagDirectRafe3;


	# def isDirectVerbRafe3(self):
		# """
		# Return True if the word is a direct Rafe3 of verb
		# @return: direct Rafe3 of verb.
		# @rtype: True/False;
		# """
		# return self.tagDirectVerbRafe3;
		
	# def isDirectNominalFactor(self):
		# """
		# Return True if the word is a direct nominal factor.
		# @return:  is a direct nominal factor.
		# @rtype: True/False;
		# """
		# return self.tagDirectNominalFactor;		
	# def isDirectAddition(self):
		# """
		# Return True if the word is a direct Addition اسم إضافة مثل نحو ومعاذ.
		# @return:  is a direct addition.
		# @rtype: True/False;
		# """
		# return self.tagDirectAddition;	

	# def isDirectVerbalFactor(self):
		# """
		# Return True if the word is a direct verbal factor.
		# @return:  is a direct verbal factor.
		# @rtype: True/False;
		# """	
		# return self.tagDirectVerbalFactor;

		
	# def isJar(self):
		# """
		# Return True if the word is a  Jar.
		# @return:  Jar.
		# @rtype: True/False;
		# """	

		# return self.tagJar;

	# def isVerbNaseb(self):
		# """
		# Return True if the word is a  Naseb of verb.
		# @return:  Naseb.
		# @rtype: True/False;
		# """	
		# return self.tagVerbNaseb;


	# def isJazem(self,):
		# """
		# Return True if the word is a  Jazem.
		# @return:  Jazem.
		# @rtype: True/False;
		# """

		# return self.tagJazem;

	# def isNaseb(self):
		# """
		# Return True if the word is a  Naseb of noun.
		# @return:  Naseb of noun.
		# @rtype: True/False;
		# """
		# return self.tagNaseb;

	# def isRafe3(self):
		# """
		# Return True if the word is a  Rafe3.
		# @return:  Rafe3.
		# @rtype: True/False;
		# """
		# return self.tagRafe3;


	# def isKanaRafe3(self):
		# """
		# Return True if the word is a  Rafe3.
		# @return:  Rafe3.
		# @rtype: True/False;
		# """
		# return self.tagKanaRafe3;

	# def isVerbRafe3(self):
		# """
		# Return True if the word is a  Rafe3 of verb
		# @return:  Rafe3 of verb.
		# @rtype: True/False;
		# """
		# return self.tagVerbRafe3;
		
	# def isNominalFactor(self):
		# """
		# Return True if the word is a  nominal factor.
		# @return:  is a  nominal factor.
		# @rtype: True/False;
		# """
		# return self.tagNominalFactor;		
	# def isAddition(self):
		# """
		# Return True if the word is a  Addition اسم إضافة مثل نحو ومعاذ.
		# @return:  is a  addition.
		# @rtype: True/False;
		# """
		# return self.tagAddition;	

	# def isKanaNoun(self):
		# """
		# Return True if the word is a  Kana Noun اسم كان منصوب.
		# @return:  is a  Kana Noun.
		# @rtype: True/False;
		# """
		# return self.tagKanaNoun;

	# def setKanaNoun(self):
		# """
		# Set True to the word to be  Kana Noun اسم كان منصوب.
		# """
		# self.tagKanaNoun = True;

	# def isInnaNoun(self):
		# """
		# Return True if the word is a  Inna Noun اسم إنّ مرفوع.
		# @return:  is a  Inna Noun.
		# @rtype: True/False;
		# """
		# return self.tagInnaNoun;

	# def setInnaNoun(self):
		# """
		# Set True to the word to be  Inna Noun اسم إنّ.
		# """
		# self.tagInnaNoun = True;

	# def isVerbalFactor(self):
		# """
		# Return True if the word is a  verbal factor.
		# @return:  is a  verbal factor.
		# @rtype: True/False;
		# """	
		# return self.tagVerbalFactor;

	# def getDict(self,):
		# syntax=u','.join(['O'+repr(self.getOrder()),self.getSyntax(), 'SP'+repr(self.semPrevious), 'SN'+repr(self.semNext)	, 'P'+repr(self.previous)	, 'N'+repr(self.next)])
		# retDict=self.__dict__
		# retDict['syntax']=syntax;
		# return retDict;
	def __repr__(self):
		text=u"\n'%s':%s, [%s]{V:%d, N:%d, S:%d} "%(self.__dict__['word'],u', '.join(self.originals), self.getWordType(), self.verbCount, self.nounCount, self.stopwordCount);
		# for k in self.__dict__.keys():
			# text += u"\t'%s':\t%s,\n "%(k,self.__dict__[k]);
		return text.encode('utf8'); 

if __name__=="__main__":
	pass;
	print "Syn Node module"
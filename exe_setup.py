#! /usr/bin/python
from distutils.core import setup
from glob import glob
import sys
sys.path.append("lib");
import py2exe
setup(name='Mishkal Softwares', version='0.2',
      description='Mishkal Softwares',
      author='Taha Zerrouki',
      author_email='taha.zerrouki@gmail.com',
      url='http://tashkeel.qutrub.org/',
      license='GPL',
	 windows = [
        {
            "script": "MishkalGui.py",
            "icon_resources": [(1, "files/favicon.png")],
			
        }
    ],

      classifiers=[
          'Development Status :: 5 - Beta',
          'Intended Audience :: End Users/Desktop',
          'Operating System :: OS independent',
          'Programming Language :: Python',
          ],
 options={"py2exe":{"includes":["sip"]}},
 
      data_files=[
	  #images
	  # ('images',[
	  # './images/logo.png','./images/sma.ico',]),
		#data
	  ('data',  [  './data/randomtext.txt',
					'./data/collocations.sqlite',
					'./data/randomtext.txt',
				] 
    	),
		#data
	  #('aranalex/data',  [  './aranalex/data/verbs.sqlite',   ]  ),

	  #docs
	  ('docs',
	   [ './docs/AUTHORS.txt',
		'./docs/THANKS.txt',
		'./docs/ChangeLog.txt',
		'./docs/COPYING.txt',
		'./docs/README.txt',
		'./docs/TODO.txt',
		'./docs/VERSION.txt',		
	   ]
       ),  
	   
	  ('ar',
	   ['./ar/style.css',
	   './ar/projects.html',
       './ar/about.html',
       './ar/help_body.html']
       ),
	  ('ar/images',
	  [	  './ar/images/exit.png',
	'./ar/images/fatha.png',
	'./ar/images/fathatan.png',
	'./ar/images/font.png',
	'./ar/images/help.jpg',
	'./ar/images/appicon.png',	
	'./ar/images/kasra.png',
	'./ar/images/logo.png',
	'./ar/images/new.png',
	'./ar/images/open.png',
	'./ar/images/paste.png',
	'./ar/images/preview.png',
	'./ar/images/print.png',
	'./ar/images/save.png',
	'./ar/images/shadda.png',
	'./ar/images/sukun.png',
	'./ar/images/zoomin.png',
	'./ar/images/zoomout.png',
	])
	# end datafiles
	  ] 
	 #end setup
 )